<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;

class PageController extends Controller
{
    public function layout(Request $request)  
    {   
        $form = new Form;
        $form->owner_name = $request->input('owner');
        $form->pipeline_name = $request->input('pipeline');
        $form->pipeline_stage = $request->input('pipeline-stage');
        $form->email = $request->input('email');
        $form->intro_message = $request->input('intro_message');
        $form->mandatory_fields = $request->input('mandatory_field');
        $form->field_details = $request->input('field_details');
        $form->add_mandatory_fields = json_encode($request->input('add_mandatory_field'));
        $form->add_items = json_encode($request->input('add_items'));
        $form->contents = json_encode($request->input('content'));
        if ($form->field_details == "") {
            $form->field_details = $request->input('field_details2');
        }
        $form->success = $request->input('success');
        $time = md5(time());
        $form->uid = $time;
        $form->save();

        return redirect('/create_form');
    }

    public function show($id) {
        
        $form_views = Form::where('UID',$id)->first();

        return view('form', compact('form_views'));

    }

    public function index() {

        $form_views = Form::all();

        return view('index', compact('form_views'));
    }

    public function edit($id) {

        $form_edits = Form::find($id);

        return view('edit', compact('form_edits'));
    }

    public function update(Request $request, $id) {

        $form = Form::find($id);

        $form->owner_name = $request->input('owner');
        $form->pipeline_name = $request->input('pipeline');
        $form->pipeline_stage = $request->input('pipeline-stage');
        $form->email = $request->input('email');
        $form->intro_message = $request->input('intro_message');
        $form->mandatory_fields = $request->input('mandatory_field');
        $form->field_details = $request->input('field_details');
        $form->add_mandatory_fields = json_encode($request->input('add_mandatory_field'));
        $form->add_items = json_encode($request->input('add_items'));
        $form->contents = json_encode($request->input('content'));
        if ($form->field_details == "") {
            $form->field_details = $request->input('field_details2');
        }
        
        $form->success = $request->input('success');
        $time = md5(time());
        $form->uid = $time;
        $form->save();

        return redirect('/index');

    }

    public function destroy($id) {

        $form = Form::find($id);
        $form->delete();

        return redirect('/index',compact('count'));
    }

}
