<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index', 'PageController@index');

Route::get('/create_form', function() {
    return view('create_form');
});

Route::post('/from', 'PageController@layout');

Route::get('/show/{id}', 'PageController@show');

Route::get('/edit/{id}', 'PageController@edit');

Route::post('/update/{id}', 'PageController@update')->name('update');

Route::get('/destroy/{id}', 'PageController@destroy')->name('destroy');

Route::get('/index2', function() {
    return view('index2');
});