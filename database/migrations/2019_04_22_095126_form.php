<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Form extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form', function(Blueprint  $table) {

            $table->increments('id');
            $table->string('owner_name');
            $table->string('pipeline_name');
            $table->string('pipeline_stage');
            $table->string('email');
            $table->text('intro_message');
            $table->string('mandatory_fields');
            $table->text('field_details');
            $table->text('add_mandatory_fields');
            $table->text('add_items');
            $table->text('contents');
            $table->text('UID');
            $table->text('success');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form');
    }
}
