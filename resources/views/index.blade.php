<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Web Form</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        
        <style>
            body {
                background-color:aliceblue;
            }
        </style>
    </head>

    <body>

        <div class="container" style="margin-top: 30px;">

            <div class="card" id="web-form-display" syle="display:block;">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-10">
                            <h2> Web Form </h2>
                        </div>
                        <div class="col-md-2 pull-right">
                        <a class="btn btn-sm btn-success" id="create-form" href="{{ url('/create_form') }}">Create Form</a>    
                        </div>    
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <p>Web forms are surveys which can be shared via a link or embedded to your web page. 
                                    The information filled will convert into deals and contacts in LMS.</p>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($form_views as $form_view)
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <b>Request Contact</b>
                                            </div>
                                            <div class="col-sm-4">    
                                            <button class="btn btn-sm" type="button" data-toggle="modal" data-target="#link-modalCenter-{{ $form_view->id }}"><i class="fas fa-external-link-alt"></i></button>
                                                &nbsp;
                                                <a href="{{ url('/edit/'.$form_view->id) }}" class="btn btn-sm btn-secondary">Edit</a>
                                                &nbsp;
                                                <form action = "{{ route('destroy', ['id'=>$form_view->id]) }}" method="GET">
                                                    <button class="fas fa-trash" type="submit"></button>
                                                </form>
                                            </div>
                                        </div>                                        
                                    </div>

                                    <div class="card-body">
                                        <p>This is a sample form. Try sharing this with your customers to create contacts in LMS.</p>
                                        <br><br>
                                        <span class="text-muted">Deals Created :</span> <br><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="text-muted">Created By:</span>
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="text-muted">Owner:</span>
                                                &nbsp; {{ $form_view->owner_name }}
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <span class="text-muted">Created At:</span>
                                                &nbsp; {{$form_view->created_at}}
                                            </div>
                                            <div class="col-sm-6">
                                                <span class="text-muted">Updated At:</span>
                                                &nbsp; {{ $form_view->updated_at }}
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="link-modalCenter-{{ $form_view->id }}" tabindex="-1" role="dialog" aria-labelledby="link-modalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 id="link-modalCenterTitle" class="modal-title">Form-Link</h5>
                                                <button type="button" class="close" data-dismiss="modal" data-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" value="http://localhost:8000/show/{{$form_view->UID}}">
                                                </div>
                                            </div>
                                            <div class="modal-header">
                                                <h5 id="link-modalCenterTitle" class="modal-title">Code-Snippet</h5>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                <textarea class="form-control" rows="6"><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"><div id="demo" data-form = "http://localhost:8000/show/{{$form_view->UID}}"></div><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script><script>$.ajax({ url: 'http://localhost:8000/show/{{$form_view->UID}}', dataType: 'html', success: function(response) { $('#demo').html(response); } });</script></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>


                </div>   
            </div>
        </div>
        
    </body>   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>              