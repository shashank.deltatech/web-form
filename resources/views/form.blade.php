<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Web Form</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background-color:aliceblue;
            }
        </style>
    </head>

    <body>

        <div class="container" style="margin-top:30px;">
            <form>
                <div class = "card" style="margin:0 auto; float:none; justify-content:center;">
                    <div class="card-header">
                    <input type="hidden" value="{{ $form_views->UID }}">
                        {{ $form_views->intro_message }}
                    </div>
                    <div class="card-body">
                        <span class="text-muted">{{ $form_views->field_details }}*</span><br><br>
                        <div class="form-group">
                            <input type="text" class="form-control">
                        </div>
                        @php
                            $content = json_decode($form_views->contents);

                            $count = count($content);
                        @endphp

                        @for($i = 0; $i < $count; $i++)
                            
                            <span class="text-muted">{{ $content[$i] }}*</span><br><br>
                            <div class="form-group">
                                <input type="text" class="form-control">
                            </div>

                        @endfor
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        
    </body>

</html>