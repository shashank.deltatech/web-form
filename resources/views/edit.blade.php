<!DOCTYPE html>

<html>

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Web Form</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            body {
                background-color:aliceblue;
            }
        </style>
    </head>

    <body>

        <div class="container" style="margin-top: 30px;">
            <div class="card" id="web-form-create">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <h2>Request Contact</h2>
                        </div>
                    </div>

                </div>
            </div>
            <form action = "{{ route('update', ['id'=>$form_edits->id]) }}" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <div class="card" style="margin-bottom:20px;" id="web-form-create2">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="margin-top: 10px;">This is a sample form. Try sharing this with your customers to create contacts in LMS.</p>
                                </div>
                            </div>   
                            <div class="row"> 
                                <div class="col-md-12">
                                    <div class="form-group">   
                                        <label for="owner" class="text-muted">OWNER*</label>
                                    </div>    
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">        
                                        <select name="owner" id="owner" class="form-control">
                                            <option value = "{{ $form_edits->owner_name }}">{{ ucfirst(strtolower($form_edits->owner_name)) }}</option>
                                        </select>
                                    </div>    
                                </div>
                            </div>          
                            <span class="text-muted"><small>Deals are created on behalf of this user</small></span><hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group"> 
                                        <label for="pipeline" class="text-muted">PIPELINE*</label>
                                    </div>    
                                </div>    
                            </div> 
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">       
                                        <select name="pipeline" id="pipeline" class="form-control">
                                            <option value="pipeline1" {{ (($form_edits->pipeline_name == "pipeline1") ? "selected" : "") }} >Pipeline 1</option>
                                            <option value="pipeline2" {{ (($form_edits->pipeline_name == "pipeline2") ? "selected" : "") }}>Pipeline 2</option>
                                            <option value="pipeline3" {{ (($form_edits->pipeline_name == "pipeline3") ? "selected" : "") }}>Pipeline 3</option>
                                        </select><br>
                                    </div>    
                                </div>    
                            </div><hr>
                            <div id="pipeline-stage">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">    
                                            <label for="pipeline-stage" class="text-muted">PIPELINE-STAGE*</label>
                                        </div>    
                                    </div>
                                </div>    
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">    
                                            <select name="pipeline-stage" class="form-control">
                                                <option value="pipeline-stage1" {{ (($form_edits->pipeline_stage == "pipeline-stage1") ? "selected" : "") }}>Pipeline Stage 1</option>
                                                <option value="pipeline-stage2" {{ (($form_edits->pipeline_stage == "pipeline-stage2") ? "selected" : "") }}>Pipeline Stage 2</option>
                                                <option value="pipeline-stage3" {{ (($form_edits->pipeline_stage == "pipeline-stage2") ? "selected" : "") }}>Pipeline Stage 3</option>
                                            </select><br>
                                        </div>    
                                    </div>    
                                </div>    
                                <span class="textmuted"><small>Deals created from this web form will appear in the selected stage and pipeline</small></span><hr>
                            </div><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="text-muted">SEND DEAL NOTIFICATIONS TO EMAIL</span>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <input type="email" placeholder="Enter an Email-Address" class="form-control" name="email" id="email" value="{{ $form_edits->email }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button class="btn btn-sm btn-danger" id="add-email">Add</button>
                                    </div>
                                </div>
                            </div>
                            <span class="text-muted" id="add-email-span">{{ $form_edits->email }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card" style="margin-top: 20px; margin-left: 20px;" id="intro-message">
                        <div class="card-header">
                            INTRODUCTION MESSAGE
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea name="intro_message" class="form-control">Please fill out the form below and one of our representatives will be in touch with you!!!</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-top:20px; margin-left: 20px;" id="mandatory-field">
                        <div class="card-header">
                            MANDATORY FIELD
                        </div>
                        <div class="card-body">
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="mandatory_field" value="person_name" id="person-name" {{ (($form_edits->mandatory_fields == "person_name") ? "checked" : "") }}>
                                <label for="person-name" class="form-check-label"> Person - Name</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" class="form-check-input" name="mandatory_field" value="organisation_name" id="organisation-name" {{ (($form_edits->mandatory_fields == "organisation_name") ? "checked" : "") }}>
                                <label for="organisation-name" class="form-check-label"> Organisation - Name</label>
                            </div>
                            <div class="form-group" id="input-person-name">
                                <input type="text" name="field_details" class="form-control" value="{{ $form_edits->field_details }}">
                            </div>
                            <div class="form-group" id="input-organisation-name" style="display:none;">
                                <input type="text" name="field_details2" class="form-control" value="{{ $form_edits->field_details2 }}">
                            </div>
                        </div>
                    </div>
                    @php
                        $field = json_decode($form_edits->add_mandatory_fields);
                        $item = json_decode($form_edits->add_items);
                        $content = json_decode($form_edits->contents);
                        
                        $count = count($field);
                    @endphp
                    @for ($i = 0; $i < $count; $i++)
                        <div class="card" style="margin-top:20px; margin-left:20px;">
                            <div class="card-header">Add Field</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select name="add_mandatory_field[]" id="add-mandatory-field" class="form-control">
                                                <option value="person" {{ (($field[$i] == "person") ? "selected" : "") }}>Person</option>
                                                <option value="organisation" {{ (($field[$i] == "organisation") ? "selected" : "") }}>Organisation</option>
                                                <option value="deal" {{ (($field[$i] == "deal") ? "selected" : "") }}>Deal</option>
                                            </select>
                                        </div>
                                    </div>
                                    @if ($field[$i] == "person")
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="add_item[]" id="add-item" class="form-control">
                                                    <option value="email" {{ (($item[$i] == "email") ? "selected" : "") }}>Email</option>
                                                    <option value="phone" {{ (($item[$i] == "phone") ? "selected" : "") }}>Phone</option>
                                                </select>
                                            </div>
                                        </div>
                                    
                                    @elseif ($field[$i] == "organisation")
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select name="add_item[]" id="add-item" class="form-control">
                                                    <option value="org_name" {{ (($item[$i] == "org_name") ? "selected" : "") }}>Name</option>
                                                    <option value="address" {{ (($item[$i] == "address") ? "selected" : "") }}>Address</option>
                                                </select>
                                            </div>
                                        </div>

                                    @else
                                        
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select name="add_item[]" id="add-item" class="form-control">
                                                <option value="title" {{ (($item[$i] == "title") ? "selected" : "") }}>Title</option>
                                                <option value="value" {{ (($item[$i] == "value") ? "selected" : "") }}>Value</option>
                                                <option value="expected_deal_closed" {{ (($item[$i] == "expected_deal_closed") ? "selected" : "") }}>Expected Deal Closed</option>
                                            </select>
                                        </div>
                                    </div>

                                    @endif
                                </div>
                                <div class="row" id="add-items-content">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name = "content[]" value={{ $content[$i] }}>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    @endfor
                    <input type="hidden" id="count" value="0">
                    <div style="margin-top:20px; margin-left: 20px;" id="mandatory-field-ajax">
                    </div> 
                    <button type="button" class="btn btn-lg btn-info" id="add-field-button" style="margin-top: 20px; margin-left:20px;">Add New Field</button>
                    <div class="card" style="margin-top: 20px; margin-left: 20px;">
                        <div class="card-header">Success Message</div>
                        <div class="card-body">
                            <div class="form-group">
                                <textarea name="success" class="form-control">Thank you for your time! We will contact you as soon as possible.</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            {{ csrf_field() }}       
            <div class="card-footer" id="footer">
                <button class="btn btn-sm btn-primary" id="save">Save</button>
            </div>
        </form>    
        </div>
    </div>

    </body>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script>

        $("input[name=mandatory_field]:radio").change(function(){
            var radioValue = $(this).val();
            if (radioValue == "person_name") {
                $('#input-person-name').show();
                document.getElementById('input-organisation-name').style.display="none";
            }else if (radioValue == "organisation_name") {
                $('#input-person-name').hide();
                document.getElementById('input-organisation-name').style.display="block";
            }else {
                $('#input-person-name').hide();
                document.getElementById('input-organisation-name').style.display="none";
            }
        });
        
        $("#add-email").on('click', function(event){
            event.preventDefault();
            var email = $("#email").val();
            $('#add-email-span').append(email+"<br>");
        });

        var count = $("#count").val();
        $('#add-field-button').on('click', function() { 
            // document.getElementById('mandatory-field-ajax').style.display="block";
            count++;
            $("#count").val(count);


            $("#mandatory-field-ajax").append('<div id="new_field_'+count+'"><div class="card"><div class="card-header">Add Field</div><div class="card-body"><div class="row"><div class="col-md-6"><div class="form-group"><select name="add_mandatory_field[]" id="add-mandatory-field_'+count+'" class="form-control" onchange="second_select('+count+')" ><option>Select Field</option><option value="person">Person</option><option value="organisation">Organisation</option><option value="deal">Deal</option></select></div></div><div class="col-md-6" id="add-person-items"><div class="form-group"><select name="add_items[]" class="form-control" id="select-items_'+count+'"><option>Select Field</option></select></div></div></div><div class="row" id="add-items-content"><div class="col-md-12"><div class="form-group"><input type="text" class="form-control" name = "content[]" placeholder="Enter content..."></div></div></div></div></div></div><br>');
        });


        function second_select(i) {
            var secondselect = $("#add-mandatory-field_"+i).val();
                $("#select-items_"+i).html("");
            if(secondselect == 'person') {
                $("#select-items_"+i).append("<option value='email'>Email</option><option value='phone'>Phone</option>");
            }else if(secondselect == 'organisation') {
                $("#select-items_"+i).append("<option value='org_name'>Name</option><option value='address'>Address</option>");
            }else {
                $("#select-items_"+i).append("<option value='title'>Title</option><option value='value'>Value</option><option value='expected_deal_closed'>Expected deal closed</option>");
            }
        }
    </script>

</html>
    